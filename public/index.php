<?php
/**
 * This is a basic web client for the framework.
 */
?>
<!DOCTYPE html>
<html lang="en_CA">
<head>
  <title><?php echo 'Some title.'; ?></title>
  <script src="js/client.js"></script>
</head>
<body>
<div class="container">
  <pre id="output"></pre>

  <form name="command" onsubmit="client.submitHandler(this); return false;">
    <input type="text" name="input" id="input" placeholder="Enter a command." />
    <input type="submit" name="submit" id="submit" value="Send" />
  </form>
</div>

<script>
var client = new CommandClient('ws://127.0.0.1:8889');
</script>
</body>
</html>

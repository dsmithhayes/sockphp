#!/usr/bin/env php
<?php

require_once 'vendor/autoload.php';

use Dsh\Sock\Sock;
use Dsh\Sock\Parser;
use Dsh\Sock\Options;
use Hoa\Websocket\Server as WsServer;
use Hoa\Socket\Server as SocketServer;
use Hoa\Event\Bucket;

// getopt loop
$shortOptions = 'va:';
$longOptions = [ 'verbose', 'address:' ];

$options = new Options(getopt($shortOptions, $longOptions));

// Command aliases
Parser::addAlias('help', ['?']);

// Build the server 
$ws = new WsServer(new SocketServer($options->getAddress()));
$sock = new Sock($ws);
$sock->setOptions($options);

if ($options->isVerbose()) {
    echo "Server running at: {$options->getAddress()}\n";

    $sock->setOpenHandler(function (Bucket $bucket) {
        echo $bucket->getData()['message'] . "\n";
    });
}

$sock->run();
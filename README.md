# SockPHP

SockPHP (or just Sock) is a framework for creating interactive message-based
WebSocket applications in PHP.

## Basic usage:

Run the WebSocket server from the root of the directory.

    $ php server.php

Run a local PHP server to run the basic client.

    $ php -S localhost:8080 -t public/

Go to `http://localhost:8080` to run the client.

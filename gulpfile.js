const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rm = require('gulp-rm');

let src = [ 'command-client.js' ];

gulp.task('clean', () => {
  return gulp.src('./public/js/client.js')
    .pipe(rm());
})

gulp.task('js', () => {
  return gulp.src(src.map((v) => { return './js/' + v }))
    .pipe(concat('client.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/js/'));
});


gulp.task('default', [ 'clean', 'js']);
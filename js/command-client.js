function CommandClient(addr) {
  this.addr = addr;
  this.ws = new WebSocket(this.addr);

  this.ws.onopen = function (e) {
    var n = Math.floor(Math.random() * 10);
    this.send(n);
    console.log('Connected to the host at: ' + addr);
  };

  this.ws.onmessage = function (m) {
    println(m.data);
  };

  this.ws.onclose = function (e) {
    console.log('Disconnect from the host.');
  };

  this.ws.onerror = function (e) {
    console.log('Error logged: ' + e.toString());
  };

  this.submitHandler = function (d) {
    var m = document.getElementById('input');
    this.ws.send(m.value);
  }

  function println(m) {
    var output = document.getElementById('output');
    output.innerHTML += m;
    output.innerHTML += "\n";
  }
}
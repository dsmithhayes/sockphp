<?php

namespace Dsh\Sock\Model;

use DateTime;
use DateTimeImmutable;
use DateInterval;

class Info
{
    protected static $connections = 0;
    protected static $startTime;

    public function __construct()
    {
        self::$startTime = new DateTime('now');
    }

    /**
     * @return $this
     */
    public function incConn()
    {
        self::$connections++;
        return $this;
    }

    /**
     * @return $this
     */
    public function decConn()
    {
        self::$connections--;
        return $this;
    }

    /**
     * @return string
     */
    public function getUptime(): string
    {
        $d = new DateTimeImmutable('now');
        $di = self::$startTime->diff($d);

        $message = "{$di->days} days, {$di->h} hours, {$di->m} minutes, {$di->s} seconds.";
        return "Uptime: {$message}";
    }
}
<?php

namespace Dsh\Sock;

class Options
{
    /**
     * @var bool
     */
    protected $isVerbose = false;

    /**
     * @var string
     */
    protected $address = 'ws://127.0.0.1:8889';

    /**
     * @param array $opts
     */
    public function __construct(array $opts)
    {
        if (isset($opts['v']) || isset($opts['verbose'])) {
            $this->setVerbose(true);
        }

        if (isset($opts['address'])) {
            $this-setAddres($opts['address']);
        }
        
        if (isset($opts['a'])) {
            $this->setAddress($opts['a']);
        }
    }

    /**
     * @param bool $v
     * @return $this
     */
    public function setVerbose(bool $v)
    {
        $this->isVerbose = $v;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVerbose(): bool
    {
        return $this->isVerbose;
    }

    /**
     * @param string $addr
     * @return $this
     */
    public function setAddress(string $addr)
    {
        $this->address = $addr;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }
}
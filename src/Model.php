<?php

namespace Dsh\Sock;

class Model
{
    public static function factory(string $name, array $params = [])
    {
        $class = "\\Dsh\\Sock\\Model\\{$name}";
        return new $class($params);
    }
}
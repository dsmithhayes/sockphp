<?php

namespace Dsh\Sock;

use Dsh\Sock\Command;

/**
 * Class Parser
 * @package Dsh\Sock
 */
class Parser
{
    /**
     * @var array
     */
    protected static $aliases = [];

    /**
     * @param string $input
     * @return \Dsh\Sock\Command
     * @throws \Exception
     */
    public static function parse(string $input): Command
    {
        $args = explode(" ", $input);
        $command = array_shift($args);

        if ($c = self::matchAlias($command)) {
            $command = $c;
        }

        $command = ucfirst($command);
        $className = "\\Dsh\\Sock\\Command\\{$command}";

        if (class_exists($className)) {
            $cmd = new $className;
            $cmd->setArgs($args);
            return $cmd;
        }

        $command = lcfirst($command);
        throw new \Exception("Command '{$command}' not found.");
    }

    /**
     * @param string $command
     * @param array $aliases
     */
    public static function addAlias(string $command, array $aliases)
    {
        if (!array_key_exists($command, self::$aliases)) {
            self::$aliases[$command] = $aliases;
        } else {
            array_merge(self::$aliases[$command], $aliases);
        }
    }

    /**
     * @param $command
     * @return bool|string
     */
    protected static function matchAlias($command)
    {
        foreach (self::$aliases as $parent => $alias) {
            foreach ($alias as $a) {
                if ($command == $a) {
                    return (string) $parent;
                }
            }
        }

        return false;
    }
}
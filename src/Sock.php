<?php

namespace Dsh\Sock;

use Dsh\Sock\Options;
use Hoa\Websocket\Server as WsServer;
use Hoa\Socket\Server as SocketServer;
use Hoa\Event\Bucket;

/**
 * Class Sock
 * @package Dsh\Sock
 */
class Sock
{
    /**
     * @var WsServer
     */
    protected $server;

    /**
     * @var Options
     */
    protected $options;

    /**
     * Sock constructor.
     * @param WsServer $server
     */
    public function __construct(WsServer $server)
    {
        $this->server = $server;
        $this->server->on('message', function (Bucket $bucket) {
            try {
                $message = $bucket->getData()['message'];

                if ($this->options->isVerbose()) {
                    echo "Received message: {$message}\n";
                }

                $command = Parser::parse($message);
                $res = $command->run();
            } catch (\Exception $e) {
                $res = $e->getMessage();
            }

            $bucket->getSource()->send($res);
        });


    }

    /**
     * @return WsServer
     */
    public function getServer(): WsServer
    {
        return $this->server;
    }

    /**
     * @param WsServer $server
     * @return $this
     */
    public function setServer(WsServer $server)
    {
        $this->server->close();
        $this->server = $server;
        return $this;
    }

    /**
     * @param callable $c
     * @return $this
     */
    public function setOpenHandler(callable $c)
    {
        $this->server->on('open', $c);
        return $this;
    }

    /**
     * @param callable $c
     * @return $this
     */
    public function setCloseHandler(callable $c)
    {
        $this->server->on('close', $c);
        return $this;
    }

    /**
     * @param Options $options
     * @return $this
     */
    public function setOptions(Options $options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return Options
     */
    public function getOptions(): Options
    {
        return $this->options;
    }

    /**
     * @return void
     */
    public function run()
    {
        $this->server->run();
    }
}
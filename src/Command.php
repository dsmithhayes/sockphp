<?php

namespace Dsh\Sock;

/**
 * Class Command
 * @package Dsh\Sock
 */
abstract class Command
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var callable
     */
    protected $action;

    /**
     * @var array
     */
    protected $args;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param callable $action
     * @return $this
     */
    public function setAction(callable $action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @param array $args
     * @return $this
     */
    public function setArgs(array $args)
    {
        $this->args = $args;
        return $this;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        return call_user_func_array($this->action, $this->args);
    }
}
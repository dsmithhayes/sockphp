<?php

namespace Dsh\Sock\Command;

use Dsh\Sock\Command;
use Dsh\Sock\Parser;

/**
 * Class Help
 * @package Dsh\Sock\Command
 */
class Help extends Command
{
    /**
     * Help constructor.
     */
    public function __construct()
    {
        $this->setName('help')
            ->setAction(function () {
                if (count($this->args)) {
                    $message = "Args passed: " . implode(", ", $this->args);
                } else {
                    $message = "No args passed.";
                }
                
                return $message;
            });
    }
}
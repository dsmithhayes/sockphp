<?php

namespace Dsh\Sock\Command;

use Dsh\Sock\Command;

class Proxy extends Command
{
    public function __construct()
    {
        $this->setName('proxy')
            ->setAction(function () {
                if (count($this->args) > 0) {
                    $buffer = file_get_contents($this->args[0]);
                } else {
                    $buffer = "Please provide an address.";
                }

                return $buffer;
            });
    }
}
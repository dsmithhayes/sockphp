<?php

namespace Dsh\Sock\Command;

use Dsh\Sock\Sock;
use Dsh\Sock\Command;

class Info extends Command
{
    public function __construct()
    {
        $this->setName('info');
        $this->setAction(function () {
            return "This is a command that gives you server information.";
        });
    }
}